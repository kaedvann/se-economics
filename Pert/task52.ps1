﻿$ScriptPath = Split-Path $MyInvocation.InvocationName
$args = @()
$args += ("-opt", 2.5)
$args += ("-pes", 4)
$args += ("-real", 3)
$args += ("-name", 'Анализ и проектирование ядра')
$cmd = "$ScriptPath\pert.ps1"

Invoke-Expression "$cmd $args"

$args = @()
$args += ("-opt", 5)
$args += ("-pes", 10)
$args += ("-real", 8)
$args += ("-name", 'Модель')
$cmd = "$ScriptPath\pert.ps1"

Invoke-Expression "$cmd $args"

$args = @()
$args += ("-opt", 0.5)
$args += ("-pes", 2)
$args += ("-real", 1)
$args += ("-name", 'Тестирование')
$cmd = "$ScriptPath\pert.ps1"

Invoke-Expression "$cmd $args"

$args = @()
$args += ("-opt", 4)
$args += ("-pes", 9)
$args += ("-real", 6)
$args += ("-name", "Создание рабочей версии ядра")
$cmd = "$ScriptPath\pert.ps1"

Invoke-Expression "$cmd $args"