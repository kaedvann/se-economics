﻿namespace Cocomo2
{
    public enum RequirementLevel
    {
        VeryLow = 0, Low = 1, Nominal = 2, High = 3, VeryHigh = 4, ExtraHigh = 5
    }
}