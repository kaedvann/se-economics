﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cocomo2
{
    class CocomoEArch
    {
        public readonly EArchParameters EArchParameters = new EArchParameters();
        public readonly PowerParameters PowerParameters = new PowerParameters();

        public double AverageDevelopers
        {
            get { return EffortApplied/DevelopmentTime; }
        }

        public double TotalCost
        {
            get
            {
                return ManMonthCost*EffortApplied;
            }
        }

        public double DevelopmentTime
        {
            get
            { //Время = 3,0 * (Работа) (0.33 + 0.2 * (p-1.01))
                return 3*Math.Pow(EffortApplied,PowerParameters.TimePower);
            }
        }


        public double Size { get; set; }
        public double ManMonthCost { get; set; }

        public double EffortApplied
        {
            get
            {
                //Работа = 2,45*ЕArch*(Размер)р
                return 2.45*EArchParameters.EArch*Math.Pow(Size, PowerParameters.Power);
            }
        }
    }
}
