﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Cocomo2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tbAveragePeople = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.tbTotalCost = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbDevTime = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbEArchCost = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbComposeCost = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbProd = new System.Windows.Forms.ComboBox();
            this.tbObjectPoints = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbManMonth = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbPrec = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cbFlex = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbResl = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cbTeam = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cbPmat = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbSced = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbPdif = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbPrex = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbFcil = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbRuse = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbRcpx = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbPers = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbCorrectedFunctionalPoints = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.fpgrid = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.requirementsgrid = new System.Windows.Forms.DataGridView();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label22 = new System.Windows.Forms.Label();
            this.tbEApp = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbPWork = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tbTimeP = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fpgrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.requirementsgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1065, 623);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DarkGray;
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1057, 597);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.DarkGray;
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.tbTimeP);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.tbPWork);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.tbEApp);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.tbAveragePeople);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.tbTotalCost);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.tbDevTime);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.tbEArchCost);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.tbComposeCost);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbManMonth);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbCorrectedFunctionalPoints);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox1.Location = new System.Drawing.Point(473, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(581, 591);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "COCOMO2";
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(300, 211);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(114, 28);
            this.label21.TabIndex = 29;
            this.label21.Text = "Среднее число разработчиков";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbAveragePeople
            // 
            this.tbAveragePeople.Location = new System.Drawing.Point(420, 216);
            this.tbAveragePeople.Name = "tbAveragePeople";
            this.tbAveragePeople.ReadOnly = true;
            this.tbAveragePeople.Size = new System.Drawing.Size(118, 20);
            this.tbAveragePeople.TabIndex = 28;
            this.tbAveragePeople.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(300, 159);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(114, 24);
            this.label20.TabIndex = 27;
            this.label20.Text = "Бюджет";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbTotalCost
            // 
            this.tbTotalCost.Location = new System.Drawing.Point(420, 162);
            this.tbTotalCost.Name = "tbTotalCost";
            this.tbTotalCost.ReadOnly = true;
            this.tbTotalCost.Size = new System.Drawing.Size(118, 20);
            this.tbTotalCost.TabIndex = 26;
            this.tbTotalCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(300, 114);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(114, 61);
            this.label19.TabIndex = 25;
            this.label19.Text = "Длительность";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbDevTime
            // 
            this.tbDevTime.Location = new System.Drawing.Point(420, 135);
            this.tbDevTime.Name = "tbDevTime";
            this.tbDevTime.ReadOnly = true;
            this.tbDevTime.Size = new System.Drawing.Size(118, 20);
            this.tbDevTime.TabIndex = 24;
            this.tbDevTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(300, 65);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(114, 61);
            this.label18.TabIndex = 23;
            this.label18.Text = "Трудозатраты по модели ранней архитектуры";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbEArchCost
            // 
            this.tbEArchCost.Location = new System.Drawing.Point(420, 86);
            this.tbEArchCost.Name = "tbEArchCost";
            this.tbEArchCost.ReadOnly = true;
            this.tbEArchCost.Size = new System.Drawing.Size(118, 20);
            this.tbEArchCost.TabIndex = 22;
            this.tbEArchCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(300, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(114, 61);
            this.label17.TabIndex = 21;
            this.label17.Text = "Трудозатраты по модели композиции";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbComposeCost
            // 
            this.tbComposeCost.Location = new System.Drawing.Point(420, 37);
            this.tbComposeCost.Name = "tbComposeCost";
            this.tbComposeCost.ReadOnly = true;
            this.tbComposeCost.Size = new System.Drawing.Size(118, 20);
            this.tbComposeCost.TabIndex = 20;
            this.tbComposeCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.cbProd);
            this.groupBox4.Controls.Add(this.tbObjectPoints);
            this.groupBox4.Location = new System.Drawing.Point(9, 508);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(248, 100);
            this.groupBox4.TabIndex = 19;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Метод композиции";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 62);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(38, 13);
            this.label16.TabIndex = 11;
            this.label16.Text = "PROD";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(2, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(109, 29);
            this.label10.TabIndex = 18;
            this.label10.Text = "Число объектных точек";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbProd
            // 
            this.cbProd.FormattingEnabled = true;
            this.cbProd.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbProd.Location = new System.Drawing.Point(78, 59);
            this.cbProd.Name = "cbProd";
            this.cbProd.Size = new System.Drawing.Size(161, 21);
            this.cbProd.TabIndex = 10;
            // 
            // tbObjectPoints
            // 
            this.tbObjectPoints.Location = new System.Drawing.Point(117, 22);
            this.tbObjectPoints.Name = "tbObjectPoints";
            this.tbObjectPoints.Size = new System.Drawing.Size(122, 20);
            this.tbObjectPoints.TabIndex = 17;
            this.tbObjectPoints.Text = "21";
            this.tbObjectPoints.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(11, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 36);
            this.label9.TabIndex = 16;
            this.label9.Text = "Стоимость человеко-месяца";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbManMonth
            // 
            this.tbManMonth.Location = new System.Drawing.Point(126, 86);
            this.tbManMonth.Name = "tbManMonth";
            this.tbManMonth.Size = new System.Drawing.Size(118, 20);
            this.tbManMonth.TabIndex = 15;
            this.tbManMonth.Text = "1000";
            this.tbManMonth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.cbPrec);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.cbFlex);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.cbResl);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.cbTeam);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.cbPmat);
            this.groupBox3.Location = new System.Drawing.Point(9, 344);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(248, 158);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Power";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 130);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "PREC";
            // 
            // cbPrec
            // 
            this.cbPrec.FormattingEnabled = true;
            this.cbPrec.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbPrec.Location = new System.Drawing.Point(78, 127);
            this.cbPrec.Name = "cbPrec";
            this.cbPrec.Size = new System.Drawing.Size(161, 21);
            this.cbPrec.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 103);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "FLEX";
            // 
            // cbFlex
            // 
            this.cbFlex.FormattingEnabled = true;
            this.cbFlex.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbFlex.Location = new System.Drawing.Point(78, 100);
            this.cbFlex.Name = "cbFlex";
            this.cbFlex.Size = new System.Drawing.Size(161, 21);
            this.cbFlex.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 76);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "RESL";
            // 
            // cbResl
            // 
            this.cbResl.FormattingEnabled = true;
            this.cbResl.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbResl.Location = new System.Drawing.Point(78, 73);
            this.cbResl.Name = "cbResl";
            this.cbResl.Size = new System.Drawing.Size(161, 21);
            this.cbResl.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 49);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "TEAM";
            // 
            // cbTeam
            // 
            this.cbTeam.FormattingEnabled = true;
            this.cbTeam.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbTeam.Location = new System.Drawing.Point(78, 46);
            this.cbTeam.Name = "cbTeam";
            this.cbTeam.Size = new System.Drawing.Size(161, 21);
            this.cbTeam.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "PMAT";
            // 
            // cbPmat
            // 
            this.cbPmat.FormattingEnabled = true;
            this.cbPmat.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbPmat.Location = new System.Drawing.Point(78, 19);
            this.cbPmat.Name = "cbPmat";
            this.cbPmat.Size = new System.Drawing.Size(161, 21);
            this.cbPmat.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.cbSced);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.cbPdif);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cbPrex);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cbFcil);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cbRuse);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.cbRcpx);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cbPers);
            this.groupBox2.Location = new System.Drawing.Point(9, 116);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(248, 222);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "EArch";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "CSED";
            // 
            // cbSced
            // 
            this.cbSced.FormattingEnabled = true;
            this.cbSced.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbSced.Location = new System.Drawing.Point(78, 181);
            this.cbSced.Name = "cbSced";
            this.cbSced.Size = new System.Drawing.Size(161, 21);
            this.cbSced.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "PDIF";
            // 
            // cbPdif
            // 
            this.cbPdif.FormattingEnabled = true;
            this.cbPdif.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbPdif.Location = new System.Drawing.Point(78, 154);
            this.cbPdif.Name = "cbPdif";
            this.cbPdif.Size = new System.Drawing.Size(161, 21);
            this.cbPdif.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "PREX";
            // 
            // cbPrex
            // 
            this.cbPrex.FormattingEnabled = true;
            this.cbPrex.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbPrex.Location = new System.Drawing.Point(78, 127);
            this.cbPrex.Name = "cbPrex";
            this.cbPrex.Size = new System.Drawing.Size(161, 21);
            this.cbPrex.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "FCIL";
            // 
            // cbFcil
            // 
            this.cbFcil.FormattingEnabled = true;
            this.cbFcil.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbFcil.Location = new System.Drawing.Point(78, 100);
            this.cbFcil.Name = "cbFcil";
            this.cbFcil.Size = new System.Drawing.Size(161, 21);
            this.cbFcil.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "RUSE";
            // 
            // cbRuse
            // 
            this.cbRuse.FormattingEnabled = true;
            this.cbRuse.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbRuse.Location = new System.Drawing.Point(78, 73);
            this.cbRuse.Name = "cbRuse";
            this.cbRuse.Size = new System.Drawing.Size(161, 21);
            this.cbRuse.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "RCPX";
            // 
            // cbRcpx
            // 
            this.cbRcpx.FormattingEnabled = true;
            this.cbRcpx.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbRcpx.Location = new System.Drawing.Point(78, 46);
            this.cbRcpx.Name = "cbRcpx";
            this.cbRcpx.Size = new System.Drawing.Size(161, 21);
            this.cbRcpx.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "PERS";
            // 
            // cbPers
            // 
            this.cbPers.FormattingEnabled = true;
            this.cbPers.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий",
            "Сверхвысокий"});
            this.cbPers.Location = new System.Drawing.Point(78, 19);
            this.cbPers.Name = "cbPers";
            this.cbPers.Size = new System.Drawing.Size(161, 21);
            this.cbPers.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 61);
            this.label1.TabIndex = 1;
            this.label1.Text = "Скорректированное количество функциональных точек";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbCorrectedFunctionalPoints
            // 
            this.tbCorrectedFunctionalPoints.Location = new System.Drawing.Point(126, 37);
            this.tbCorrectedFunctionalPoints.Name = "tbCorrectedFunctionalPoints";
            this.tbCorrectedFunctionalPoints.ReadOnly = true;
            this.tbCorrectedFunctionalPoints.Size = new System.Drawing.Size(118, 20);
            this.tbCorrectedFunctionalPoints.TabIndex = 0;
            this.tbCorrectedFunctionalPoints.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGray;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.fpgrid);
            this.panel1.Controls.Add(this.requirementsgrid);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(460, 591);
            this.panel1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(169, 549);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Рассчитать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // fpgrid
            // 
            this.fpgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fpgrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.NullValue = "0";
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.fpgrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.fpgrid.Location = new System.Drawing.Point(-4, -4);
            this.fpgrid.Name = "fpgrid";
            this.fpgrid.Size = new System.Drawing.Size(464, 145);
            this.fpgrid.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Низкий";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Средний";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Высокий";
            this.Column3.Name = "Column3";
            // 
            // requirementsgrid
            // 
            this.requirementsgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.requirementsgrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column4});
            this.requirementsgrid.Location = new System.Drawing.Point(-4, 144);
            this.requirementsgrid.Name = "requirementsgrid";
            this.requirementsgrid.Size = new System.Drawing.Size(464, 382);
            this.requirementsgrid.TabIndex = 1;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4.HeaderText = "Значение";
            this.Column4.Name = "Column4";
            this.Column4.Width = 80;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1057, 597);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(300, 258);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(114, 28);
            this.label22.TabIndex = 31;
            this.label22.Text = "EArch";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbEApp
            // 
            this.tbEApp.Location = new System.Drawing.Point(420, 263);
            this.tbEApp.Name = "tbEApp";
            this.tbEApp.ReadOnly = true;
            this.tbEApp.Size = new System.Drawing.Size(118, 20);
            this.tbEApp.TabIndex = 30;
            this.tbEApp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(300, 290);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(114, 28);
            this.label23.TabIndex = 33;
            this.label23.Text = "Work P";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbPWork
            // 
            this.tbPWork.Location = new System.Drawing.Point(420, 295);
            this.tbPWork.Name = "tbPWork";
            this.tbPWork.ReadOnly = true;
            this.tbPWork.Size = new System.Drawing.Size(118, 20);
            this.tbPWork.TabIndex = 32;
            this.tbPWork.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(300, 318);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(114, 28);
            this.label24.TabIndex = 35;
            this.label24.Text = "Time P";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbTimeP
            // 
            this.tbTimeP.Location = new System.Drawing.Point(420, 323);
            this.tbTimeP.Name = "tbTimeP";
            this.tbTimeP.ReadOnly = true;
            this.tbTimeP.Size = new System.Drawing.Size(118, 20);
            this.tbTimeP.TabIndex = 34;
            this.tbTimeP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1065, 623);
            this.Controls.Add(this.tabControl1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fpgrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.requirementsgrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private TabPage tabPage1;
        private DataGridView fpgrid;
        private TabPage tabPage2;
        private DataGridViewTextBoxColumn Column1;
        private DataGridViewTextBoxColumn Column2;
        private DataGridViewTextBoxColumn Column3;
        private Panel panel1;
        private DataGridView requirementsgrid;
        private DataGridViewTextBoxColumn Column4;
        private GroupBox groupBox1;
        private Label label1;
        private TextBox tbCorrectedFunctionalPoints;
        private Button button1;
        private GroupBox groupBox2;
        private Label label4;
        private ComboBox cbRuse;
        private Label label3;
        private ComboBox cbRcpx;
        private Label label2;
        private ComboBox cbPers;
        private GroupBox groupBox3;
        private Label label11;
        private ComboBox cbPrec;
        private Label label12;
        private ComboBox cbFlex;
        private Label label13;
        private ComboBox cbResl;
        private Label label14;
        private ComboBox cbTeam;
        private Label label15;
        private ComboBox cbPmat;
        private Label label8;
        private ComboBox cbSced;
        private Label label5;
        private ComboBox cbPdif;
        private Label label6;
        private ComboBox cbPrex;
        private Label label7;
        private ComboBox cbFcil;
        private Label label9;
        private TextBox tbManMonth;
        private Label label10;
        private TextBox tbObjectPoints;
        private GroupBox groupBox4;
        private Label label16;
        private ComboBox cbProd;
        private Label label21;
        private TextBox tbAveragePeople;
        private Label label20;
        private TextBox tbTotalCost;
        private Label label19;
        private TextBox tbDevTime;
        private Label label18;
        private TextBox tbEArchCost;
        private Label label17;
        private TextBox tbComposeCost;
        private Label label24;
        private TextBox tbTimeP;
        private Label label23;
        private TextBox tbPWork;
        private Label label22;
        private TextBox tbEApp;
    }
}

