﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cocomo2
{
    class CocomoCompose
    {
        public int ObjectPointsCount { get; set; }
        public RequirementLevel ProdLevel { get; set; }
        public double EffortApplied { get { return Nop/Prod; } }
        public double Nop
        {
            get { return ObjectPointsCount*(100 - Ruse)/100; }
        }

        public double Ruse { get; set; }


        public double Prod
        {
            get
            {
                switch (ProdLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 4;
                    case RequirementLevel.Low:
                        return 7;
                    case RequirementLevel.Nominal:
                        return 13;
                    case RequirementLevel.High:
                        return 25;
                    case RequirementLevel.VeryHigh:
                        return 50;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}
