﻿using System;
using System.Windows.Forms;

namespace Cocomo2
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            InitializeGrids();
            DefaultData();
        }

        private double FunctionalPoints()
        {
            var result = 0.0;
            result += Convert.ToInt32(fpgrid.Rows[0].Cells[0].Value) * 3;
            result += Convert.ToInt32(fpgrid.Rows[0].Cells[1].Value) * 4;
            result += Convert.ToInt32(fpgrid.Rows[0].Cells[2].Value) * 6;
            result += Convert.ToInt32(fpgrid.Rows[1].Cells[0].Value) * 4;
            result += Convert.ToInt32(fpgrid.Rows[1].Cells[1].Value) * 5;
            result += Convert.ToInt32(fpgrid.Rows[1].Cells[2].Value) * 7;
            result += Convert.ToInt32(fpgrid.Rows[2].Cells[0].Value) * 3;
            result += Convert.ToInt32(fpgrid.Rows[2].Cells[1].Value) * 4;
            result += Convert.ToInt32(fpgrid.Rows[2].Cells[2].Value) * 6;
            result += Convert.ToInt32(fpgrid.Rows[3].Cells[0].Value) * 7;
            result += Convert.ToInt32(fpgrid.Rows[3].Cells[1].Value) * 11;
            result += Convert.ToInt32(fpgrid.Rows[3].Cells[2].Value) * 15;
            result += Convert.ToInt32(fpgrid.Rows[4].Cells[0].Value) * 5;
            result += Convert.ToInt32(fpgrid.Rows[4].Cells[1].Value) * 7;
            result += Convert.ToInt32(fpgrid.Rows[4].Cells[2].Value) * 11;
            return result;
        }

        private double CorrectedFunctionalPoints()
        {
            var rowSum = 0.0;
            for (int i = 0; i < requirementsgrid.RowCount; ++i)
                rowSum += 0.01 * int.Parse(requirementsgrid.Rows[i].Cells[0].Value.ToString());
            return FunctionalPoints()*(0.65 + rowSum);
        }

        private void InitializeGrids()
        {
            fpgrid.RowCount = 6;
            fpgrid.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            fpgrid.AllowUserToAddRows = false;
            fpgrid.Rows[0].HeaderCell.Value = "Внешние вводы";
            fpgrid.Rows[1].HeaderCell.Value = "Внешние выводы";
            fpgrid.Rows[2].HeaderCell.Value = "Внешние запросы";
            fpgrid.Rows[3].HeaderCell.Value = "Внутренние файлы";
            fpgrid.Rows[4].HeaderCell.Value = "Внешние файлы";

            requirementsgrid.RowCount = 15;
            requirementsgrid.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            requirementsgrid.AllowUserToAddRows = false;
            requirementsgrid.Rows[0].HeaderCell.Value = "Обмен данными";
            requirementsgrid.Rows[1].HeaderCell.Value = "Распределенная обработка";
            requirementsgrid.Rows[2].HeaderCell.Value = "Производительность  ";
            requirementsgrid.Rows[3].HeaderCell.Value = "Эксплуатационные ограничения по аппаратным ресурсам ";
            requirementsgrid.Rows[4].HeaderCell.Value = "Транзакционная нагрузка ";
            requirementsgrid.Rows[5].HeaderCell.Value = "Интенсивность взаимодействия с пользователем ";
            requirementsgrid.Rows[6].HeaderCell.Value = "Эргономические характеристики";
            requirementsgrid.Rows[7].HeaderCell.Value = "Оперативное обновление ";
            requirementsgrid.Rows[8].HeaderCell.Value = "Сложность обработки ";
            requirementsgrid.Rows[9].HeaderCell.Value = "Легкость эксплуатации/администрирования ";
            requirementsgrid.Rows[10].HeaderCell.Value = " Повторное использование ";
            requirementsgrid.Rows[11].HeaderCell.Value = "Легкость инсталляции ";
            requirementsgrid.Rows[12].HeaderCell.Value = "Портируемость ";
            requirementsgrid.Rows[13].HeaderCell.Value = "Гибкость";

        }

        private void DefaultData()
        {
            fpgrid.Rows[0].Cells[0].Value = 10;
            fpgrid.Rows[1].Cells[0].Value = 7;
            fpgrid.Rows[2].Cells[0].Value = 2;
            fpgrid.Rows[2].Cells[1].Value = 1;
            fpgrid.Rows[3].Cells[0].Value = 1;
            fpgrid.Rows[4].Cells[0].Value = 3;

            requirementsgrid.Rows[0].Cells[0].Value = 5;
            requirementsgrid.Rows[1].Cells[0].Value = 5;
            requirementsgrid.Rows[2].Cells[0].Value = 3;
            requirementsgrid.Rows[3].Cells[0].Value = 0;
            requirementsgrid.Rows[4].Cells[0].Value = 3;
            requirementsgrid.Rows[5].Cells[0].Value = 2;
            requirementsgrid.Rows[6].Cells[0].Value = 0;
            requirementsgrid.Rows[7].Cells[0].Value = 4;
            requirementsgrid.Rows[8].Cells[0].Value = 4;
            requirementsgrid.Rows[9].Cells[0].Value = 3;
            requirementsgrid.Rows[10].Cells[0].Value = 3;
            requirementsgrid.Rows[11].Cells[0].Value = 0;
            requirementsgrid.Rows[12].Cells[0].Value = 5;
            requirementsgrid.Rows[13].Cells[0].Value = 0;

            cbSced.SelectedIndex = (int)RequirementLevel.Low;
            cbFcil.SelectedIndex = (int)RequirementLevel.VeryHigh;
            cbFlex.SelectedIndex = (int)RequirementLevel.VeryLow;
            cbPdif.SelectedIndex = (int)RequirementLevel.Nominal;
            cbPers.SelectedIndex = (int)RequirementLevel.VeryHigh;
            cbPmat.SelectedIndex = (int)RequirementLevel.High;
            cbPrec.SelectedIndex = (int)RequirementLevel.Low;
            cbRcpx.SelectedIndex = (int)RequirementLevel.VeryHigh;
            cbTeam.SelectedIndex = (int)RequirementLevel.High;
            cbPrex.SelectedIndex = (int)RequirementLevel.Low;
            cbResl.SelectedIndex = (int)RequirementLevel.High;
            cbRuse.SelectedIndex = (int)RequirementLevel.Low;
            cbProd.SelectedIndex = (int) RequirementLevel.High;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var functionalPointsCount = CorrectedFunctionalPoints();
                tbCorrectedFunctionalPoints.Text = functionalPointsCount.ToString("F2");
                UpdateParameters();
                UpdateView();
            }
            catch
            {
                MessageBox.Show(@"Неверные данные");
            }
        }


        private readonly CocomoCompose _cocomoCompose = new CocomoCompose();
        private readonly CocomoEArch _cocomoEArch = new CocomoEArch();

        private void UpdateView()
        {
            tbAveragePeople.Text = _cocomoEArch.AverageDevelopers.ToString("F2");
            tbComposeCost.Text = _cocomoCompose.EffortApplied.ToString("F2");
            tbEArchCost.Text = _cocomoEArch.EffortApplied.ToString("F2");
            tbDevTime.Text = _cocomoEArch.DevelopmentTime.ToString("F2");
            tbTotalCost.Text = _cocomoEArch.TotalCost.ToString("F2");
            tbEApp.Text = _cocomoEArch.EArchParameters.EArch.ToString("F2");
            tbPWork.Text = _cocomoEArch.PowerParameters.Power.ToString("F2");
            tbTimeP.Text = _cocomoEArch.PowerParameters.TimePower.ToString("F2");
        }
        private void UpdateParameters()
        {
            _cocomoCompose.ObjectPointsCount = int.Parse(tbObjectPoints.Text);
            _cocomoCompose.ProdLevel = (RequirementLevel) cbProd.SelectedIndex;
            _cocomoCompose.Ruse = 0;

            _cocomoEArch.EArchParameters.FcilLevel = (RequirementLevel)cbFcil.SelectedIndex;
            _cocomoEArch.EArchParameters.PdifLevel = (RequirementLevel)cbPdif.SelectedIndex;
            _cocomoEArch.EArchParameters.PersLevel = (RequirementLevel)cbPers.SelectedIndex;
            _cocomoEArch.EArchParameters.PrexLevel = (RequirementLevel)cbPrex.SelectedIndex;
            _cocomoEArch.EArchParameters.RpcxLevel = (RequirementLevel)cbRcpx.SelectedIndex;
            _cocomoEArch.EArchParameters.RuseLevel = (RequirementLevel)cbRuse.SelectedIndex;
            _cocomoEArch.EArchParameters.ScedLevel = (RequirementLevel)cbSced.SelectedIndex;
            _cocomoEArch.Size = double.Parse(tbCorrectedFunctionalPoints.Text);

            _cocomoEArch.PowerParameters.FlexLevel = (RequirementLevel)cbFlex.SelectedIndex;
            _cocomoEArch.PowerParameters.PmatLevel = (RequirementLevel)cbPmat.SelectedIndex;
            _cocomoEArch.PowerParameters.PrecLevel = (RequirementLevel)cbPrec.SelectedIndex;
            _cocomoEArch.PowerParameters.ReslLevel = (RequirementLevel)cbResl.SelectedIndex;
            _cocomoEArch.PowerParameters.TeamLevel = (RequirementLevel)cbTeam.SelectedIndex;
            _cocomoEArch.ManMonthCost = double.Parse(tbManMonth.Text);
        }
    }
}