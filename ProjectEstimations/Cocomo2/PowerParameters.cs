﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cocomo2
{
    class PowerParameters
    {
        public RequirementLevel PrecLevel { get; set; }
        public RequirementLevel PmatLevel { get; set; }
        public RequirementLevel TeamLevel { get; set; }
        public RequirementLevel ReslLevel { get; set; }
        public RequirementLevel FlexLevel { get; set; }

        public double TimePower
        {
            get { return 0.33 + 0.2 * (Power - 1.01); }
        }
        public double Prec {
            get
            {
                switch (PrecLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 6.2;
                    case RequirementLevel.Low:
                        return 4.96;
                    case RequirementLevel.Nominal:
                        return 3.72;
                    case RequirementLevel.High:
                        return 2.48;
                    case RequirementLevel.VeryHigh:
                        return 1.24;
                    case RequirementLevel.ExtraHigh:
                        return 0;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        public double Pmat { get
            {
                switch (PmatLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 7;
                    case RequirementLevel.Low:
                        return 6.24;
                    case RequirementLevel.Nominal:
                        return 4.68;
                    case RequirementLevel.High:
                        return 1.12;
                    case RequirementLevel.VeryHigh:
                        return 1.56;
                    case RequirementLevel.ExtraHigh:
                        return 0;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
          }
        public double Team { get
            {
                switch (TeamLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 5.48;
                    case RequirementLevel.Low:
                        return 4.38;
                    case RequirementLevel.Nominal:
                        return 3.29;
                    case RequirementLevel.High:
                        return 2.19;
                    case RequirementLevel.VeryHigh:
                        return 1.1;
                    case RequirementLevel.ExtraHigh:
                        return 0;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
          }
        public double Resl { get
            {
                switch (ReslLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 7;
                    case RequirementLevel.Low:
                        return 5.65;
                    case RequirementLevel.Nominal:
                        return 4.24;
                    case RequirementLevel.High:
                        return 2.83;
                    case RequirementLevel.VeryHigh:
                        return 1.41;
                    case RequirementLevel.ExtraHigh:
                        return 0;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
          }
        public double Flex
        {
            get
            {
                switch (FlexLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 5.07;
                    case RequirementLevel.Low:
                        return 4.05;
                    case RequirementLevel.Nominal:
                        return 3.04;
                    case RequirementLevel.High:
                        return 2.03;
                    case RequirementLevel.VeryHigh:
                        return 1.01;
                    case RequirementLevel.ExtraHigh:
                        return 0;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Power
        {
            get { return 1.1 + (Prec + Pmat + Team + Resl + Flex)/100.0; }
        }                      
    }                          
}                              
                               