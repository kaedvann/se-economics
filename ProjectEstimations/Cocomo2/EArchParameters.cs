﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cocomo2
{
    class EArchParameters
    {
        public RequirementLevel PersLevel { get; set; }
        public RequirementLevel RpcxLevel { get; set; }
        public RequirementLevel RuseLevel { get; set; }
        public RequirementLevel FcilLevel { get; set; }
        public RequirementLevel PrexLevel { get; set; }
        public RequirementLevel PdifLevel { get; set; }
        public RequirementLevel ScedLevel { get; set; }


        public double EArch {
            get { return Pers*Rpcx*Ruse*Fcil*Prex*Pdif*Sced; }
        }
        public double Pers {
            get {
                switch (PersLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 1.62;
                        
                    case RequirementLevel.Low:
                        return 1.26;
                        
                    case RequirementLevel.Nominal:
                        return 1;
                        
                    case RequirementLevel.High:
                        return 0.83;
                        
                    case RequirementLevel.VeryHigh:
                        return 0.63;
                        
                    case RequirementLevel.ExtraHigh:
                        return 0.5;
                        
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            } 
        }
        public double Rpcx
        {
            get
            {
                switch (RpcxLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 0.6;
                        
                    case RequirementLevel.Low:
                        return 0.83;
                        
                    case RequirementLevel.Nominal:
                        return 1;
                        
                    case RequirementLevel.High:
                        return 1.33;
                        
                    case RequirementLevel.VeryHigh:
                        return 1.91;
                        
                    case RequirementLevel.ExtraHigh:
                        return 2.72;
                        
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        public double Ruse
        {
            get
            {
                switch (RuseLevel)
                {
                    case RequirementLevel.Low:
                        return 0.95;
                        
                    case RequirementLevel.Nominal:
                        return 1;
                        
                    case RequirementLevel.High:
                        return 1.07;
                        
                    case RequirementLevel.VeryHigh:
                        return 1.15;
                        
                    case RequirementLevel.ExtraHigh:
                        return 1.24;
                        
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        public double Fcil
        {
            get
            {
                switch (FcilLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 1.30;
                        
                    case RequirementLevel.Low:
                        return 1.10;
                        
                    case RequirementLevel.Nominal:
                        return 1;
                        
                    case RequirementLevel.High:
                        return 0.87;
                        
                    case RequirementLevel.VeryHigh:
                        return 0.73;
                        
                    case RequirementLevel.ExtraHigh:
                        return 0.62;
                        
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        public double Prex
        {
            get
            {
                switch (PrexLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 1.33;
                        
                    case RequirementLevel.Low:
                        return 1.22;
                        
                    case RequirementLevel.Nominal:
                        return 1;
                        
                    case RequirementLevel.High:
                        return 0.87;
                        
                    case RequirementLevel.VeryHigh:
                        return 0.74;
                        
                    case RequirementLevel.ExtraHigh:
                        return 0.62;
                        
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        public double Pdif
        {
            get
            {
                switch (PdifLevel)
                {
                    case RequirementLevel.Low:
                        return 0.87;
                        
                    case RequirementLevel.Nominal:
                        return 1;
                        
                    case RequirementLevel.High:
                        return 1.29;
                        
                    case RequirementLevel.VeryHigh:
                        return 1.81;
                        
                    case RequirementLevel.ExtraHigh:
                        return 2.61;
                        
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        public double Sced
        {
            get
            {
                switch (ScedLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 1.43;
                        
                    case RequirementLevel.Low:
                        return 1.14;
                        
                    case RequirementLevel.Nominal:
                        return 1;
                        
                    case RequirementLevel.High:
                        return 1;
                        
                    case RequirementLevel.VeryHigh:
                        return 1;
                        
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

    }
}
