﻿namespace ProjectEstamations
{
    public enum CocomoType
    {
        Organic,
        SemiDetached,
        Embedded
    }
}