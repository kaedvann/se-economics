﻿using System;
using System.Linq;

namespace ProjectEstamations
{
    public class CocomoModel
    {


        private static readonly double[] WbsStagesCost = {0.04,0.12,0.44,0.06,0.14,0.07,0.07,0.06};
        private static readonly double[] StagesWork = {0.08,0.18,0.25,0.26,0.31 };
        private static readonly double[] StagesTime = {0.36,0.36,0.18,0.18,0.28};

        public double[] WbsStagesFinalCost { get { return WbsStagesCost.Select(d => d*TotalCost).ToArray();} }

        public double[] StageMonths { get { return StagesTime.Select(t => t*DevelopmentTime).ToArray(); } }

        public double[] StageEfforts { get { return StagesWork.Select(c => c*EffortApplied).ToArray(); } }

        public double[] PeoplePerStage { get { return StagesManMonth.Select(((d, i) => d/StageMonths[i])).ToArray();} }

        public double[] StagesManMonth { get { return StagesWork.Select(t => t*EffortApplied).ToArray(); } }

        public readonly EafParameters EafParameters = new EafParameters();
        public int KiloLinesOfCode { get; set; }
        public CocomoType CocomoType { get; set; }

        public double EffortApplied
        {
            get { return C1*EafParameters.Eaf*Math.Pow(KiloLinesOfCode, P1); }
        }

        public double TotalEffort { get { return EffortApplied*1.08; } }

        public double AveragePeople { get { return EffortApplied/DevelopmentTime; } }

        public double MonthCost { get; set; }

        public double TotalCost { get { return MonthCost*TotalEffort; } }

        public double C1
        {
            get
            {
                switch (CocomoType)
                {
                    case CocomoType.Organic:
                        return 3.2;
                    case CocomoType.SemiDetached:
                        return 3.0;
                    case CocomoType.Embedded:
                        return 2.8;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double[] PeopleDistribution
        {
            get
            {
                var result = new double[(int) TotalTime];
                for (int i = 0; i < result.Length; ++i)
                {
                    result[i] = PeoplePerStage[StageMonths.IndexByElementsSum(i)];
                }
                return result;
            }
        }

        public double P1
        {
            get
            {
                switch (CocomoType)
                {
                    case CocomoType.Organic:
                        return 1.05;
                    case CocomoType.SemiDetached:
                        return 1.12;
                    case CocomoType.Embedded:
                        return 1.2;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double DevelopmentTime
        {
            get { return C2*Math.Pow(EffortApplied, P2); }
        }

        public double TotalTime { get { return DevelopmentTime*1.36;} }

        public double P2
        {
            get
            {
                switch (CocomoType)
                {
                    case CocomoType.Organic:
                        return 0.38;
                    case CocomoType.SemiDetached:
                        return 0.35;
                    case CocomoType.Embedded:
                        return 0.32;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double C2
        {
            get { return 2.5; }
        }
    }
}