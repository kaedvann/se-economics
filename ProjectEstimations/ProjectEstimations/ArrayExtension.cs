﻿using System.Linq;

namespace ProjectEstamations
{
    public static class ArrayExtension
    {
        public static int IndexByElementsSum(this double[] array, int sumNotLessThan)
        {
            int result = 0;
            while (array.Take(result + 1).Sum() <= sumNotLessThan && result != array.Length)
            {
                ++result;
            }
            return result;
        }
    }
}