﻿namespace ProjectEstamations
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tbSum = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbMonthCost = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.barMen = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label4 = new System.Windows.Forms.Label();
            this.edtKLOC = new System.Windows.Forms.TextBox();
            this.edtWork = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.edtTime = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.edtMenAve = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.edtMenMax = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.rbtnIns = new System.Windows.Forms.RadioButton();
            this.rbtnMid = new System.Windows.Forms.RadioButton();
            this.rbtnMain = new System.Windows.Forms.RadioButton();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cbxEAF_Sced = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Tool = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Modp = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Lexp = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Vexp = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Pcap = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Aexp = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Acap = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Turn = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Virt = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Stor = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Time = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Cplx = new System.Windows.Forms.ComboBox();
            this.cbxEAF_Data = new System.Windows.Forms.ComboBox();
            this.Label16 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label15 = new System.Windows.Forms.Label();
            this.cbxEAF_Rely = new System.Windows.Forms.ComboBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableDistr = new System.Windows.Forms.DataGridView();
            this.label22 = new System.Windows.Forms.Label();
            this.tableWBS = new System.Windows.Forms.DataGridView();
            this.label21 = new System.Windows.Forms.Label();
            this.CodeType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Budget = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MenMon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barMen)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.TableLayoutPanel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableDistr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableWBS)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1912, 991);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.tableLayoutPanel3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.GroupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Size = new System.Drawing.Size(1904, 958);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Общее";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.tbSum, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label26, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.tbMonthCost, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label25, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.barMen, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.edtKLOC, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.edtWork, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.label17, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.edtTime, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.label18, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.label23, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.edtMenAve, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.label24, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.edtMenMax, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.button1, 1, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(4, 77);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 19;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.555555F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1329, 876);
            this.tableLayoutPanel3.TabIndex = 4;
            // 
            // tbSum
            // 
            this.tbSum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSum.Location = new System.Drawing.Point(1000, 10);
            this.tbSum.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbSum.Name = "tbSum";
            this.tbSum.ReadOnly = true;
            this.tbSum.Size = new System.Drawing.Size(325, 26);
            this.tbSum.TabIndex = 20;
            this.tbSum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(908, 13);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(84, 20);
            this.label26.TabIndex = 19;
            this.label26.Text = "Бюджет";
            // 
            // tbMonthCost
            // 
            this.tbMonthCost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMonthCost.Location = new System.Drawing.Point(336, 56);
            this.tbMonthCost.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbMonthCost.Name = "tbMonthCost";
            this.tbMonthCost.Size = new System.Drawing.Size(324, 26);
            this.tbMonthCost.TabIndex = 18;
            this.tbMonthCost.Text = "1000";
            this.tbMonthCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(50, 59);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(278, 20);
            this.label25.TabIndex = 17;
            this.label25.Text = "Стоимость человеко-месяца";
            // 
            // barMen
            // 
            chartArea1.AxisX.MajorGrid.Interval = 0D;
            chartArea1.AxisX.MajorGrid.IntervalOffset = 0D;
            chartArea1.AxisX.MajorGrid.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea1.AxisX.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea1.AxisX.MajorTickMark.Enabled = false;
            chartArea1.AxisY.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea1.AxisY.MajorGrid.Enabled = false;
            chartArea1.AxisY.MajorGrid.Interval = 0D;
            chartArea1.AxisY.MajorGrid.IntervalOffset = 0D;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            chartArea1.Name = "ChartArea1";
            this.barMen.ChartAreas.Add(chartArea1);
            this.tableLayoutPanel3.SetColumnSpan(this.barMen, 4);
            this.barMen.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.barMen.Legends.Add(legend1);
            this.barMen.Location = new System.Drawing.Point(4, 235);
            this.barMen.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.barMen.Name = "barMen";
            this.tableLayoutPanel3.SetRowSpan(this.barMen, 14);
            series1.BorderWidth = 4;
            series1.ChartArea = "ChartArea1";
            series1.CustomProperties = "LabelStyle=TopRight";
            series1.Legend = "Legend1";
            series1.Name = "Количество человек";
            this.barMen.Series.Add(series1);
            this.barMen.Size = new System.Drawing.Size(1321, 636);
            this.barMen.TabIndex = 11;
            this.barMen.Text = "chart1";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(269, 13);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "KLOC";
            // 
            // edtKLOC
            // 
            this.edtKLOC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.edtKLOC.Location = new System.Drawing.Point(336, 10);
            this.edtKLOC.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.edtKLOC.Name = "edtKLOC";
            this.edtKLOC.Size = new System.Drawing.Size(324, 26);
            this.edtKLOC.TabIndex = 3;
            this.edtKLOC.Text = "25";
            this.edtKLOC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // edtWork
            // 
            this.edtWork.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.edtWork.Location = new System.Drawing.Point(1000, 56);
            this.edtWork.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.edtWork.Name = "edtWork";
            this.edtWork.ReadOnly = true;
            this.edtWork.Size = new System.Drawing.Size(325, 26);
            this.edtWork.TabIndex = 6;
            this.edtWork.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(916, 59);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 20);
            this.label17.TabIndex = 4;
            this.label17.Text = "Работа";
            // 
            // edtTime
            // 
            this.edtTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.edtTime.Location = new System.Drawing.Point(1000, 102);
            this.edtTime.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.edtTime.Name = "edtTime";
            this.edtTime.ReadOnly = true;
            this.edtTime.Size = new System.Drawing.Size(325, 26);
            this.edtTime.TabIndex = 7;
            this.edtTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(924, 105);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 20);
            this.label18.TabIndex = 5;
            this.label18.Text = "Время";
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(801, 151);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(191, 20);
            this.label23.TabIndex = 12;
            this.label23.Text = "Человек в среднем";
            // 
            // edtMenAve
            // 
            this.edtMenAve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.edtMenAve.Location = new System.Drawing.Point(1000, 148);
            this.edtMenAve.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.edtMenAve.Name = "edtMenAve";
            this.edtMenAve.ReadOnly = true;
            this.edtMenAve.Size = new System.Drawing.Size(325, 26);
            this.edtMenAve.TabIndex = 13;
            this.edtMenAve.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(808, 197);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(184, 20);
            this.label24.TabIndex = 14;
            this.label24.Text = "Человек максимум";
            // 
            // edtMenMax
            // 
            this.edtMenMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.edtMenMax.Location = new System.Drawing.Point(1000, 194);
            this.edtMenMax.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.edtMenMax.Name = "edtMenMax";
            this.edtMenMax.ReadOnly = true;
            this.edtMenMax.Size = new System.Drawing.Size(325, 26);
            this.edtMenMax.TabIndex = 15;
            this.edtMenMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(336, 97);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(324, 35);
            this.button1.TabIndex = 16;
            this.button1.Text = "Рассчитать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(4, 5);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(1329, 72);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Вариант";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.rbtnIns, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.rbtnMid, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.rbtnMain, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 24);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1321, 43);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // rbtnIns
            // 
            this.rbtnIns.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbtnIns.AutoSize = true;
            this.rbtnIns.Location = new System.Drawing.Point(1037, 9);
            this.rbtnIns.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtnIns.Name = "rbtnIns";
            this.rbtnIns.Size = new System.Drawing.Size(127, 24);
            this.rbtnIns.TabIndex = 2;
            this.rbtnIns.Text = "Встроенный";
            this.rbtnIns.UseVisualStyleBackColor = true;
            // 
            // rbtnMid
            // 
            this.rbtnMid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbtnMid.AutoSize = true;
            this.rbtnMid.Checked = true;
            this.rbtnMid.Location = new System.Drawing.Point(581, 9);
            this.rbtnMid.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtnMid.Name = "rbtnMid";
            this.rbtnMid.Size = new System.Drawing.Size(158, 24);
            this.rbtnMid.TabIndex = 1;
            this.rbtnMid.TabStop = true;
            this.rbtnMid.Text = "Промежуточный";
            this.rbtnMid.UseVisualStyleBackColor = true;
            // 
            // rbtnMain
            // 
            this.rbtnMain.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rbtnMain.AutoSize = true;
            this.rbtnMain.Location = new System.Drawing.Point(166, 9);
            this.rbtnMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rbtnMain.Name = "rbtnMain";
            this.rbtnMain.Size = new System.Drawing.Size(108, 24);
            this.rbtnMain.TabIndex = 0;
            this.rbtnMain.Text = "Основной";
            this.rbtnMain.UseVisualStyleBackColor = true;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.TableLayoutPanel1);
            this.GroupBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.GroupBox1.Location = new System.Drawing.Point(1333, 5);
            this.GroupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GroupBox1.Size = new System.Drawing.Size(567, 948);
            this.GroupBox1.TabIndex = 2;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Параметры EAF";
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.ColumnCount = 2;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Sced, 1, 14);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Tool, 1, 13);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Modp, 1, 12);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Lexp, 1, 11);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Vexp, 1, 10);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Pcap, 1, 9);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Aexp, 1, 8);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Acap, 1, 7);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Turn, 1, 6);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Virt, 1, 5);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Stor, 1, 4);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Time, 1, 3);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Cplx, 1, 2);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Data, 1, 1);
            this.TableLayoutPanel1.Controls.Add(this.Label16, 0, 7);
            this.TableLayoutPanel1.Controls.Add(this.Label14, 0, 6);
            this.TableLayoutPanel1.Controls.Add(this.Label12, 0, 5);
            this.TableLayoutPanel1.Controls.Add(this.Label10, 0, 4);
            this.TableLayoutPanel1.Controls.Add(this.Label8, 0, 3);
            this.TableLayoutPanel1.Controls.Add(this.Label2, 0, 1);
            this.TableLayoutPanel1.Controls.Add(this.Label3, 0, 2);
            this.TableLayoutPanel1.Controls.Add(this.Label5, 0, 8);
            this.TableLayoutPanel1.Controls.Add(this.Label6, 0, 9);
            this.TableLayoutPanel1.Controls.Add(this.Label7, 0, 10);
            this.TableLayoutPanel1.Controls.Add(this.Label9, 0, 11);
            this.TableLayoutPanel1.Controls.Add(this.Label11, 0, 12);
            this.TableLayoutPanel1.Controls.Add(this.Label13, 0, 13);
            this.TableLayoutPanel1.Controls.Add(this.Label15, 0, 14);
            this.TableLayoutPanel1.Controls.Add(this.cbxEAF_Rely, 1, 0);
            this.TableLayoutPanel1.Controls.Add(this.Label1, 0, 0);
            this.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel1.Location = new System.Drawing.Point(4, 24);
            this.TableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 15;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(559, 919);
            this.TableLayoutPanel1.TabIndex = 0;
            // 
            // cbxEAF_Sced
            // 
            this.cbxEAF_Sced.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Sced.FormattingEnabled = true;
            this.cbxEAF_Sced.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Sced.Location = new System.Drawing.Point(283, 872);
            this.cbxEAF_Sced.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Sced.Name = "cbxEAF_Sced";
            this.cbxEAF_Sced.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Sced.TabIndex = 29;
            // 
            // cbxEAF_Tool
            // 
            this.cbxEAF_Tool.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Tool.FormattingEnabled = true;
            this.cbxEAF_Tool.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Tool.Location = new System.Drawing.Point(283, 809);
            this.cbxEAF_Tool.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Tool.Name = "cbxEAF_Tool";
            this.cbxEAF_Tool.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Tool.TabIndex = 28;
            // 
            // cbxEAF_Modp
            // 
            this.cbxEAF_Modp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Modp.FormattingEnabled = true;
            this.cbxEAF_Modp.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Modp.Location = new System.Drawing.Point(283, 748);
            this.cbxEAF_Modp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Modp.Name = "cbxEAF_Modp";
            this.cbxEAF_Modp.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Modp.TabIndex = 27;
            // 
            // cbxEAF_Lexp
            // 
            this.cbxEAF_Lexp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Lexp.FormattingEnabled = true;
            this.cbxEAF_Lexp.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Lexp.Location = new System.Drawing.Point(283, 687);
            this.cbxEAF_Lexp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Lexp.Name = "cbxEAF_Lexp";
            this.cbxEAF_Lexp.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Lexp.TabIndex = 26;
            // 
            // cbxEAF_Vexp
            // 
            this.cbxEAF_Vexp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Vexp.FormattingEnabled = true;
            this.cbxEAF_Vexp.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Vexp.Location = new System.Drawing.Point(283, 626);
            this.cbxEAF_Vexp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Vexp.Name = "cbxEAF_Vexp";
            this.cbxEAF_Vexp.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Vexp.TabIndex = 25;
            // 
            // cbxEAF_Pcap
            // 
            this.cbxEAF_Pcap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Pcap.FormattingEnabled = true;
            this.cbxEAF_Pcap.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Pcap.Location = new System.Drawing.Point(283, 565);
            this.cbxEAF_Pcap.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Pcap.Name = "cbxEAF_Pcap";
            this.cbxEAF_Pcap.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Pcap.TabIndex = 24;
            // 
            // cbxEAF_Aexp
            // 
            this.cbxEAF_Aexp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Aexp.FormattingEnabled = true;
            this.cbxEAF_Aexp.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Aexp.Location = new System.Drawing.Point(283, 504);
            this.cbxEAF_Aexp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Aexp.Name = "cbxEAF_Aexp";
            this.cbxEAF_Aexp.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Aexp.TabIndex = 23;
            // 
            // cbxEAF_Acap
            // 
            this.cbxEAF_Acap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Acap.FormattingEnabled = true;
            this.cbxEAF_Acap.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Acap.Location = new System.Drawing.Point(283, 443);
            this.cbxEAF_Acap.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Acap.Name = "cbxEAF_Acap";
            this.cbxEAF_Acap.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Acap.TabIndex = 22;
            // 
            // cbxEAF_Turn
            // 
            this.cbxEAF_Turn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Turn.FormattingEnabled = true;
            this.cbxEAF_Turn.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Turn.Location = new System.Drawing.Point(283, 382);
            this.cbxEAF_Turn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Turn.Name = "cbxEAF_Turn";
            this.cbxEAF_Turn.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Turn.TabIndex = 21;
            // 
            // cbxEAF_Virt
            // 
            this.cbxEAF_Virt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Virt.FormattingEnabled = true;
            this.cbxEAF_Virt.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Virt.Location = new System.Drawing.Point(283, 321);
            this.cbxEAF_Virt.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Virt.Name = "cbxEAF_Virt";
            this.cbxEAF_Virt.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Virt.TabIndex = 20;
            // 
            // cbxEAF_Stor
            // 
            this.cbxEAF_Stor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Stor.FormattingEnabled = true;
            this.cbxEAF_Stor.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Stor.Location = new System.Drawing.Point(283, 260);
            this.cbxEAF_Stor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Stor.Name = "cbxEAF_Stor";
            this.cbxEAF_Stor.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Stor.TabIndex = 19;
            // 
            // cbxEAF_Time
            // 
            this.cbxEAF_Time.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Time.FormattingEnabled = true;
            this.cbxEAF_Time.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Time.Location = new System.Drawing.Point(283, 199);
            this.cbxEAF_Time.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Time.Name = "cbxEAF_Time";
            this.cbxEAF_Time.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Time.TabIndex = 18;
            // 
            // cbxEAF_Cplx
            // 
            this.cbxEAF_Cplx.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Cplx.FormattingEnabled = true;
            this.cbxEAF_Cplx.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Cplx.Location = new System.Drawing.Point(283, 138);
            this.cbxEAF_Cplx.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Cplx.Name = "cbxEAF_Cplx";
            this.cbxEAF_Cplx.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Cplx.TabIndex = 17;
            // 
            // cbxEAF_Data
            // 
            this.cbxEAF_Data.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Data.FormattingEnabled = true;
            this.cbxEAF_Data.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Data.Location = new System.Drawing.Point(283, 77);
            this.cbxEAF_Data.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Data.Name = "cbxEAF_Data";
            this.cbxEAF_Data.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Data.TabIndex = 16;
            // 
            // Label16
            // 
            this.Label16.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label16.AutoSize = true;
            this.Label16.Location = new System.Drawing.Point(82, 447);
            this.Label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(193, 20);
            this.Label16.TabIndex = 14;
            this.Label16.Text = "Способности аналитика";
            // 
            // Label14
            // 
            this.Label14.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label14.AutoSize = true;
            this.Label14.Location = new System.Drawing.Point(53, 386);
            this.Label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(222, 20);
            this.Label14.TabIndex = 12;
            this.Label14.Text = "Время реакции компьютера";
            // 
            // Label12
            // 
            this.Label12.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(50, 315);
            this.Label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(225, 40);
            this.Label12.TabIndex = 10;
            this.Label12.Text = "Изменчивость виртуальной машины";
            // 
            // Label10
            // 
            this.Label10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(25, 254);
            this.Label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(250, 40);
            this.Label10.TabIndex = 8;
            this.Label10.Text = "Ограничение объёма основной памяти";
            // 
            // Label8
            // 
            this.Label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(93, 193);
            this.Label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(182, 40);
            this.Label8.TabIndex = 6;
            this.Label8.Text = "Ограничение времени выполнения";
            // 
            // Label2
            // 
            this.Label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(109, 81);
            this.Label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(166, 20);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "Размер базы данных";
            // 
            // Label3
            // 
            this.Label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(106, 142);
            this.Label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(169, 20);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "Сложность продукта";
            // 
            // Label5
            // 
            this.Label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(113, 508);
            this.Label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(162, 20);
            this.Label5.TabIndex = 3;
            this.Label5.Text = "Знание приложений";
            // 
            // Label6
            // 
            this.Label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(54, 569);
            this.Label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(221, 20);
            this.Label6.TabIndex = 4;
            this.Label6.Text = "Способности программиста";
            // 
            // Label7
            // 
            this.Label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(42, 630);
            this.Label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(233, 20);
            this.Label7.TabIndex = 5;
            this.Label7.Text = "Знание виртуальной машины";
            // 
            // Label9
            // 
            this.Label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(11, 691);
            this.Label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(264, 20);
            this.Label9.TabIndex = 7;
            this.Label9.Text = "Знание языка программирования";
            // 
            // Label11
            // 
            this.Label11.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(40, 742);
            this.Label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(235, 40);
            this.Label11.TabIndex = 9;
            this.Label11.Text = "Использование современных методов";
            // 
            // Label13
            // 
            this.Label13.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label13.AutoSize = true;
            this.Label13.Location = new System.Drawing.Point(39, 803);
            this.Label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(236, 40);
            this.Label13.TabIndex = 11;
            this.Label13.Text = "Использование программных инструментов";
            // 
            // Label15
            // 
            this.Label15.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label15.AutoSize = true;
            this.Label15.Location = new System.Drawing.Point(44, 876);
            this.Label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(231, 20);
            this.Label15.TabIndex = 13;
            this.Label15.Text = "Требуемые сроки разработки";
            // 
            // cbxEAF_Rely
            // 
            this.cbxEAF_Rely.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxEAF_Rely.FormattingEnabled = true;
            this.cbxEAF_Rely.Items.AddRange(new object[] {
            "Очень низкий",
            "Низкий",
            "Номинальный",
            "Высокий",
            "Очень высокий"});
            this.cbxEAF_Rely.Location = new System.Drawing.Point(283, 16);
            this.cbxEAF_Rely.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEAF_Rely.Name = "cbxEAF_Rely";
            this.cbxEAF_Rely.Size = new System.Drawing.Size(272, 28);
            this.cbxEAF_Rely.TabIndex = 15;
            // 
            // Label1
            // 
            this.Label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(88, 20);
            this.Label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(187, 20);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Требуемая надёжность";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.tableLayoutPanel4);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Size = new System.Drawing.Size(1904, 958);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Таблицы";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Controls.Add(this.tableDistr, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.label22, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.tableWBS, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label21, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(4, 5);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 10;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.999203F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.9992F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.9992F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.9992F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.9992F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.9992F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0012F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0012F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0012F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0012F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1896, 948);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // tableDistr
            // 
            this.tableDistr.AllowUserToAddRows = false;
            this.tableDistr.AllowUserToDeleteRows = false;
            this.tableDistr.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tableDistr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableDistr.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.Column2,
            this.dataGridViewTextBoxColumn3,
            this.Column1});
            this.tableLayoutPanel4.SetColumnSpan(this.tableDistr, 4);
            this.tableDistr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableDistr.Location = new System.Drawing.Point(4, 569);
            this.tableDistr.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableDistr.Name = "tableDistr";
            this.tableLayoutPanel4.SetRowSpan(this.tableDistr, 4);
            this.tableDistr.Size = new System.Drawing.Size(1888, 374);
            this.tableDistr.TabIndex = 3;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label22, 4);
            this.label22.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label22.Location = new System.Drawing.Point(4, 538);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(1888, 26);
            this.label22.TabIndex = 2;
            this.label22.Text = "Распределение работ и времени по стадиям жизненного цикла при традиционном подход" +
    "е";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableWBS
            // 
            this.tableWBS.AllowUserToAddRows = false;
            this.tableWBS.AllowUserToDeleteRows = false;
            this.tableWBS.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tableWBS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableWBS.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodeType,
            this.Budget,
            this.MenMon});
            this.tableLayoutPanel4.SetColumnSpan(this.tableWBS, 4);
            this.tableWBS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableWBS.Location = new System.Drawing.Point(4, 99);
            this.tableWBS.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableWBS.Name = "tableWBS";
            this.tableLayoutPanel4.SetRowSpan(this.tableWBS, 4);
            this.tableWBS.Size = new System.Drawing.Size(1888, 366);
            this.tableWBS.TabIndex = 0;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label21, 4);
            this.label21.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label21.Location = new System.Drawing.Point(4, 68);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(1888, 26);
            this.label21.TabIndex = 1;
            this.label21.Text = "Стандартное распределение работ по видам деятельности WBS в модели COCOMO";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CodeType
            // 
            this.CodeType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CodeType.DefaultCellStyle = dataGridViewCellStyle4;
            this.CodeType.Frozen = true;
            this.CodeType.HeaderText = "Тип кода";
            this.CodeType.Name = "CodeType";
            this.CodeType.ReadOnly = true;
            this.CodeType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CodeType.Width = 83;
            // 
            // Budget
            // 
            this.Budget.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Budget.DefaultCellStyle = dataGridViewCellStyle5;
            this.Budget.HeaderText = "Бюджет (%)";
            this.Budget.Name = "Budget";
            this.Budget.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Budget.Width = 106;
            // 
            // MenMon
            // 
            this.MenMon.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.Format = "G5";
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MenMon.DefaultCellStyle = dataGridViewCellStyle6;
            this.MenMon.HeaderText = "Бюджет";
            this.MenMon.Name = "MenMon";
            this.MenMon.ReadOnly = true;
            this.MenMon.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "Вид деятельности";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Width = 144;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn2.HeaderText = "Работа (%)";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 88;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Работа(человеко-месяцы)";
            this.Column2.Name = "Column2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.Format = "G5";
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn3.HeaderText = "Время (%)";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 83;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Время (месяцы)";
            this.Column1.Name = "Column1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1912, 991);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.Text = "COCOMO";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barMen)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.TableLayoutPanel1.ResumeLayout(false);
            this.TableLayoutPanel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableDistr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableWBS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edtKLOC;
        private System.Windows.Forms.TextBox edtWork;
        internal System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox edtTime;
        internal System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.RadioButton rbtnIns;
        private System.Windows.Forms.RadioButton rbtnMid;
        private System.Windows.Forms.RadioButton rbtnMain;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal System.Windows.Forms.ComboBox cbxEAF_Sced;
        internal System.Windows.Forms.ComboBox cbxEAF_Tool;
        internal System.Windows.Forms.ComboBox cbxEAF_Modp;
        internal System.Windows.Forms.ComboBox cbxEAF_Lexp;
        internal System.Windows.Forms.ComboBox cbxEAF_Vexp;
        internal System.Windows.Forms.ComboBox cbxEAF_Pcap;
        internal System.Windows.Forms.ComboBox cbxEAF_Aexp;
        internal System.Windows.Forms.ComboBox cbxEAF_Acap;
        internal System.Windows.Forms.ComboBox cbxEAF_Turn;
        internal System.Windows.Forms.ComboBox cbxEAF_Virt;
        internal System.Windows.Forms.ComboBox cbxEAF_Stor;
        internal System.Windows.Forms.ComboBox cbxEAF_Time;
        internal System.Windows.Forms.ComboBox cbxEAF_Cplx;
        internal System.Windows.Forms.ComboBox cbxEAF_Data;
        internal System.Windows.Forms.Label Label16;
        internal System.Windows.Forms.Label Label14;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label Label15;
        internal System.Windows.Forms.ComboBox cbxEAF_Rely;
        internal System.Windows.Forms.Label Label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataVisualization.Charting.Chart barMen;
        private System.Windows.Forms.DataGridView tableWBS;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridView tableDistr;
        private System.Windows.Forms.Label label22;
        internal System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox edtMenAve;
        internal System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox edtMenMax;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbSum;
        internal System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbMonthCost;
        internal System.Windows.Forms.Label label25;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodeType;
        private System.Windows.Forms.DataGridViewTextBoxColumn Budget;
        private System.Windows.Forms.DataGridViewTextBoxColumn MenMon;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;

    }
}

