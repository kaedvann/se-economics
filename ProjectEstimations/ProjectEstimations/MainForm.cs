﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace ProjectEstamations
{
    public partial class MainForm : Form
    {
        private readonly CocomoModel _cocomo = new CocomoModel();
        private RadioButton[] _radios;

        void DefaultParametres()
        {
            cbxEAF_Acap.SelectedIndex = 2;
            cbxEAF_Aexp.SelectedIndex = 2;
            cbxEAF_Cplx.SelectedIndex = 2;
            cbxEAF_Data.SelectedIndex = 2;
            cbxEAF_Lexp.SelectedIndex = 3;
            cbxEAF_Modp.SelectedIndex = 4;
            cbxEAF_Pcap.SelectedIndex = 3;
            cbxEAF_Rely.SelectedIndex = 2;
            cbxEAF_Sced.SelectedIndex = 2;
            cbxEAF_Stor.SelectedIndex = 2;
            cbxEAF_Time.SelectedIndex = 2;
            cbxEAF_Tool.SelectedIndex = 4;
            cbxEAF_Turn.SelectedIndex = 2;
            cbxEAF_Vexp.SelectedIndex = 2;
            cbxEAF_Virt.SelectedIndex = 2;

            tableWBS.Rows.Add("Анализ требований", "4", "");
            tableWBS.Rows.Add("Проектирование продукта", "12", "");
            tableWBS.Rows.Add("Программирование", "44");
            tableWBS.Rows.Add("Планирование тестирования", "6", "");
            tableWBS.Rows.Add("Верификация и аттестация", "14", "");
            tableWBS.Rows.Add("Канцелярия проекта", "7", "");
            tableWBS.Rows.Add("Управление конфигурацией и обеспечение качества", "7", "");
            tableWBS.Rows.Add("Создание руководств", "6", "");
            tableWBS.Rows.Add("Итого", "100", "");

            tableDistr.Rows.Add("Планирование и определение требований", "8","", "36");
            tableDistr.Rows.Add("Проектирование продукта", "18", "", "36");
            tableDistr.Rows.Add("Детальное проектирование", "25", "", "18");
            tableDistr.Rows.Add("Кодирование и тестирование отдельных модулей", "26", "", "18");
            tableDistr.Rows.Add("Интеграция и тестирование", "31", "", "28");
            tableDistr.Rows.Add("Всего", "108", "", "136");

            barMen.Series[0].SmartLabelStyle.Enabled = false;

            _radios = new[] {rbtnMain, rbtnMid, rbtnIns};
        }

        public MainForm()
        {
            InitializeComponent();

            DefaultParametres();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateCocomo();
                UpdateView();
            }
            catch
            {
                MessageBox.Show(@"Некорректные данные");
            }
        }

        private void UpdateView()
        {
            tbSum.Text = _cocomo.TotalCost.ToString("F2");
            edtWork.Text = _cocomo.TotalEffort.ToString("F2");
            edtTime.Text = _cocomo.TotalTime.ToString("F2");
            edtMenAve.Text = _cocomo.AveragePeople.ToString("F1");
            edtMenMax.Text = _cocomo.PeopleDistribution.Max().ToString("F1");
            barMen.Series[0].Points.Clear();
            for (int i = 0; i < _cocomo.PeopleDistribution.Length; ++i)
            {
                barMen.Series[0].Points.AddXY(i+1, _cocomo.PeopleDistribution[i]);
            }

            for (int i = 0; i < _cocomo.WbsStagesFinalCost.Length; ++i)
            {
                tableWBS.Rows[i].Cells[2].Value = _cocomo.WbsStagesFinalCost[i];
            }
            for (int i = 0; i < _cocomo.StageMonths.Length; ++i)
            {
                tableDistr.Rows[i].Cells[4].Value = _cocomo.StageMonths[i].ToString("F2");
                tableDistr.Rows[i].Cells[2].Value = _cocomo.StageEfforts[i].ToString("F2");
            }
            tableWBS.Rows[_cocomo.WbsStagesFinalCost.Length].Cells[2].Value = _cocomo.WbsStagesFinalCost.Sum().ToString("F2");
            tableDistr.Rows[_cocomo.StageMonths.Length].Cells[2].Value = _cocomo.StageEfforts.Sum().ToString("F2");
            tableDistr.Rows[_cocomo.StageMonths.Length].Cells[4].Value = _cocomo.StageMonths.Sum().ToString("F2");
        }

        private void UpdateCocomo()
        {
            _cocomo.EafParameters.AcapLevel = (RequirementLevel) cbxEAF_Acap.SelectedIndex;
            _cocomo.EafParameters.AexpLevel = (RequirementLevel) cbxEAF_Aexp.SelectedIndex;
            _cocomo.EafParameters.CplxLevel = (RequirementLevel) cbxEAF_Cplx.SelectedIndex;

            _cocomo.EafParameters.DataLevel = (RequirementLevel)cbxEAF_Data.SelectedIndex;
            _cocomo.EafParameters.LexpLevel = (RequirementLevel)cbxEAF_Lexp.SelectedIndex;
            _cocomo.EafParameters.ModpLevel = (RequirementLevel)cbxEAF_Modp.SelectedIndex;

            _cocomo.EafParameters.PcapLevel = (RequirementLevel)cbxEAF_Pcap.SelectedIndex;
            _cocomo.EafParameters.RelyLevel = (RequirementLevel)cbxEAF_Rely.SelectedIndex;
            _cocomo.EafParameters.ScedLevel = (RequirementLevel)cbxEAF_Sced.SelectedIndex;

            _cocomo.EafParameters.StorLevel = (RequirementLevel)cbxEAF_Stor.SelectedIndex;
            _cocomo.EafParameters.TimeLevel = (RequirementLevel)cbxEAF_Time.SelectedIndex;
            _cocomo.EafParameters.ToolLevel = (RequirementLevel)cbxEAF_Tool.SelectedIndex;

            _cocomo.EafParameters.VirtLevel = (RequirementLevel)cbxEAF_Virt.SelectedIndex;
            _cocomo.EafParameters.VexpLevel = (RequirementLevel)cbxEAF_Vexp.SelectedIndex;
            _cocomo.EafParameters.TurnLevel = (RequirementLevel)cbxEAF_Turn.SelectedIndex;
            _cocomo.CocomoType = (CocomoType) Array.FindIndex(_radios, r => r.Checked);
            _cocomo.KiloLinesOfCode = int.Parse(edtKLOC.Text);
            _cocomo.MonthCost = double.Parse(tbMonthCost.Text);

        }
    }
}
