﻿using System;

namespace ProjectEstamations
{
    public class EafParameters
    {
        public RequirementLevel RelyLevel { get; set; }
        public RequirementLevel DataLevel { get; set; }
        public RequirementLevel CplxLevel { get; set; }
        public RequirementLevel TimeLevel { get; set; }
        public RequirementLevel StorLevel { get; set; }
        public RequirementLevel VirtLevel { get; set; }
        public RequirementLevel TurnLevel { get; set; }
        public RequirementLevel AcapLevel { get; set; }
        public RequirementLevel AexpLevel { get; set; }
        public RequirementLevel PcapLevel { get; set; }
        public RequirementLevel VexpLevel { get; set; }
        public RequirementLevel LexpLevel { get; set; }
        public RequirementLevel ModpLevel { get; set; }
        public RequirementLevel ToolLevel { get; set; }
        public RequirementLevel ScedLevel { get; set; }

        public double Eaf
        {
            get { return Rely*Data*Cplx*Time*Stor*Virt*Turn*Acap*Aexp*Pcap*Vexp*Lexp*Modp*Tool*Sced; }
        }

        public double Rely
        {
            get
            {
                switch (RelyLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 0.75;
                    case RequirementLevel.Low:
                        return 0.86;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 1.15;
                    case RequirementLevel.VeryHigh:
                        return 1.4;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }


        public double Data
        {
            get
            {
                switch (DataLevel)
                {
                    case RequirementLevel.Low:
                        return 0.94;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 1.08;
                    case RequirementLevel.VeryHigh:
                        return 1.16;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Cplx
        {
            get
            {
                switch (CplxLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 0.7;
                    case RequirementLevel.Low:
                        return 0.85;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 1.15;
                    case RequirementLevel.VeryHigh:
                        return 1.3;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Time
        {
            get
            {
                switch (TimeLevel)
                {
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 1.11;
                    case RequirementLevel.VeryHigh:
                        return 1.5;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Stor
        {
            get
            {
                switch (StorLevel)
                {
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 1.06;
                    case RequirementLevel.VeryHigh:
                        return 1.21;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Virt
        {
            get
            {
                switch (VirtLevel)
                {
                    case RequirementLevel.Low:
                        return 0.87;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 1.15;
                    case RequirementLevel.VeryHigh:
                        return 1.3;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Turn
        {
            get
            {
                switch (TurnLevel)
                {
                    case RequirementLevel.Low:
                        return 0.87;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 1.07;
                    case RequirementLevel.VeryHigh:
                        return 1.15;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Acap
        {
            get
            {
                switch (AcapLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 1.46;
                    case RequirementLevel.Low:
                        return 1.19;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 0.86;
                    case RequirementLevel.VeryHigh:
                        return 0.71;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Aexp
        {
            get
            {
                switch (AexpLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 1.29;
                    case RequirementLevel.Low:
                        return 1.15;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 0.91;
                    case RequirementLevel.VeryHigh:
                        return 0.82;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Pcap
        {
            get
            {
                switch (PcapLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 1.42;
                    case RequirementLevel.Low:
                        return 1.17;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 0.86;
                    case RequirementLevel.VeryHigh:
                        return 0.7;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Vexp
        {
            get
            {
                switch (VexpLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 1.21;
                    case RequirementLevel.Low:
                        return 1.1;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 0.9;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Lexp
        {
            get
            {
                switch (LexpLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 1.14;
                    case RequirementLevel.Low:
                        return 1.07;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 0.95;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Modp
        {
            get
            {
                switch (ModpLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 1.24;
                    case RequirementLevel.Low:
                        return 1.1;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 0.91;
                    case RequirementLevel.VeryHigh:
                        return 0.82;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Tool
        {
            get
            {
                switch (ToolLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 1.24;
                    case RequirementLevel.Low:
                        return 1.1;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 0.91;
                    case RequirementLevel.VeryHigh:
                        return 0.82;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public double Sced
        {
            get
            {
                switch (ScedLevel)
                {
                    case RequirementLevel.VeryLow:
                        return 1.23;
                    case RequirementLevel.Low:
                        return 1.08;
                    case RequirementLevel.Nominal:
                        return 1.0;
                    case RequirementLevel.High:
                        return 1.04;
                    case RequirementLevel.VeryHigh:
                        return 1.1;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}